/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define DT_DRV_COMPAT onnn_ax5043

#include <device.h>
#include <drivers/gpio.h>
#include <drivers/spi.h>
#include <pm/device.h>

//#include "ax5043_driver.h"
//#include <stm32l4xx_hal.h>
#include <main.h>
#include <conf.h>
//#include "utils.h"
#include <radio.h>
#include <ax5043.h>
#include <ax5043_driver.h>
//#include <string.h>

#define SPI_TIMEOUT      1000
//extern SPI_HandleTypeDef hspi2;

struct ax5043_drv_cfg {
//	struct gpio_dt_spec cs_spec;
	struct gpio_dt_spec irq_spec;
	struct spi_dt_spec bus;
};

struct ax5043_drv_data {
	struct gpio_callback irq_gpio_cb;
};

#define RILDOS_BUFFER_LEN (10 * 2048)

//static struct ax5043_drv_data ax5043_ctl = {
//	.cs_spec = GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(ax5043), cs_gpios, 0)
//	.irq_spec = GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(ax5043), irq_gpios, 0)
//};

static uint8_t ax5043_rx[MAX_RX_FRAME_LEN];
static uint8_t ax5043_tx[RILDOS_BUFFER_LEN + 1024];

static int ax5043_spi_cs(uint8_t cs);
static int ax5043_spi_trx(uint8_t *rx, const uint8_t *tx, uint32_t len);
static int ax5043_spi_rx(uint8_t *rx, uint32_t len);
static int ax5043_spi_tx(const uint8_t *tx, uint32_t len);
static void ax5043_usleep(uint32_t us);
static uint32_t ax5043_millis(void);
static void ax5043_irq_enable(bool enable);
static void ax5043_tx_pre_callback(void);
static void ax5043_tx_post_callback(void);
static void ax5043_rx_complete_callback(const uint8_t *pdu, size_t len);

static struct ax5043_conf hax5043 = {
	/* Configure XTAL and VCO */
	.vco = VCO_INTERNAL,
	.xtal = {
		.freq = QUBIK_XTAL_FREQ_HZ,
		.type = TCXO,
		.capacitance = 3,
	},
	/* Setup AX5043 driver internal buffers */
	.rx_buffer = ax5043_rx,
	.rx_buffer_len = MAX_RX_FRAME_LEN,
	.tx_buffer = ax5043_tx,
	.tx_buffer_len = RILDOS_BUFFER_LEN + 1024,
	.ops = {
		.spi_cs = ax5043_spi_cs,
		.spi_trx = ax5043_spi_trx,
		.spi_rx = ax5043_spi_rx,
		.spi_tx = ax5043_spi_tx,
		.usleep = ax5043_usleep,
		.millis = ax5043_millis,
		.irq_enable = ax5043_irq_enable,
		.tx_pre_callback = ax5043_tx_pre_callback,
		.tx_post_callback = ax5043_tx_post_callback,
		.rx_complete_callback = ax5043_rx_complete_callback,
	},
//	.os_data = &ax5043_drv_data,
};

static void ax5043_irq_callback_handler(const struct device *port,
					struct gpio_callback *cb,
					gpio_port_pins_t pins)
{
	ax5043_irq_callback();
}

static int ax5043_configure_irq(const struct device *dev)
{
	const struct ax5043_drv_cfg *config = dev->config;
	struct ax5043_drv_data *data = dev->data;
	int ret;

	ret = gpio_pin_configure_dt(&config->irq_spec, GPIO_INPUT);
	if (ret < 0) {
		return ret;
	}

	gpio_init_callback(&data->irq_gpio_cb,
			   ax5043_irq_callback_handler,
			   BIT(config->irq_spec.pin));

	ret = gpio_add_callback(config->irq_spec.port, &data->irq_gpio_cb);
	if (ret < 0) {
		return ret;
	}

#warning: trigger?
	ret = gpio_pin_interrupt_configure_dt(&config->irq_spec,
					      GPIO_INT_EDGE_TO_ACTIVE);
	if (ret < 0) {
		return ret;
	}

	return 0;
}

static int ax5043_driver_init(const struct device *dev)
{
	const struct ax5043_drv_cfg *config = dev->config;
	int ret;

	if (!config->irq_spec.port/* || !config->cs_spec.port*/) {
		return -ENODEV;
	}
#if 0
	ax5043_drv_data.cs_dev = device_get_binding(AX5043_SEL_NAME);
	ax5043_drv_data.irq_dev = device_get_binding(AX5043_IRQ_NAME);
	if (!ax5043_drv_data.cs_dev || !ax5043_drv_data.irq_dev) {
		return -ENODEV;
	}
#endif
	ret = ax5043_init(&hax5043);
	if (ret < 0)
		return ret;

	return ax5043_configure_irq(dev);
}

#ifdef CONFIG_PM_DEVICE
static int ax5043_pm_action(const struct device *dev,
			    enum pm_device_action action)
{
	return 0;
}
#endif

#warning SPI_MODE_CPOL and SPI_MODE_CPHA have to be clarified. FreeRTOS uses
/*
 *	hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
 *	hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
 */

#define AX5043_INIT(n)							\
	static const struct ax5043_drv_cfg ax5043_cfg_##n = {		\
		/*.cs_spec = GPIO_DT_SPEC_INST_GET(n, spi_cs_gpios),*/	\
		.irq_spec = GPIO_DT_SPEC_INST_GET(n, irq_gpios),	\
		.bus = SPI_DT_SPEC_GET(DT_DRV_INST(n),			\
			SPI_OP_MODE_MASTER | SPI_TRANSFER_MSB |		\
			SPI_WORD_SET(8), 0),				\
	};								\
									\
	static struct ax5043_drv_data ax5043_data_##n;			\
									\
	PM_DEVICE_DT_INST_DEFINE(n, ax5043_pm_action);			\
									\
	DEVICE_DT_INST_DEFINE(n, ax5043_driver_init,			\
			      PM_DEVICE_DT_INST_GET(n),			\
			      &ax5043_data_##n,				\
			      &ax5043_cfg_##n, POST_KERNEL,		\
			      20, NULL);

DT_INST_FOREACH_STATUS_OKAY(AX5043_INIT)

/**
 * SPI chip select pin level
 * @param cs set 1 to set the CS signal high, 0 to set it low
 * @return 0 on success or negative error code
 *
 * Note, that the CS is actually handled by the SPI driver automatically, this
 * function is only needed for resetting. Further note, that the parameter
 * actually means the pin level, not its active or inactive state. We actually
 * have to check the pin configuration, but we hard-code active-low for now. 
 */
static int ax5043_spi_cs(uint8_t cs)
{
	//HAL_GPIO_WritePin(AX5043_SEL_GPIO_Port, AX5043_SEL_Pin, cs & 0x1);
	gpio_pin_set_dt(&ax5043_cfg_0.bus.config.cs->gpio, cs & 1);
//	spi_context_cs_control(&ax5043_cfg_0.bus, !cs);
//	gpio_pin_configure_dt(&ax5043_cfg_0.cs_spec,
//			      cs & 1 ? GPIO_OUTPUT_HIGH : GPIO_OUTPUT_LOW);
	return 0;
}

/**
 * Writes and reads data from the IC using the SPI
 * @param rx buffer to store the received data
 * @param tx buffer containing the data to transmit
 * @param len the number of bytes to be sent and received
 * @return 0 on success or negative error code
 */
static int ax5043_spi_trx(uint8_t *rx, const uint8_t *tx, uint32_t len)
{
//	return HAL_SPI_TransmitReceive(&hspi2, tx, rx, len, SPI_TIMEOUT);;

	/*
	 * Casting const away isn't good, but it's a transmit buffer, it really
	 * won't be modified, it's just the API, that uses the same structure
	 * for transmission and reception
	 */
	const struct spi_buf tx_buf = {
		.buf = (uint8_t *)tx,
		.len = len,
	};
	const struct spi_buf_set tx_set = {
		.buffers = &tx_buf,
		.count = 1,
	};
	const struct spi_buf rx_buf = {
		.buf = rx,
		.len = len,
	};
	const struct spi_buf_set rx_set = {
		.buffers = &rx_buf,
		.count = 1,
	};

	return spi_transceive_dt(&ax5043_cfg_0.bus, &tx_set, &rx_set);
}

/**
 * Reads data from the IC using the SPI
 * @param rx buffer to store the received data
 * @param len the number of bytes to be sent and received
 * @return 0 on success or negative error code
 */
static int ax5043_spi_rx(uint8_t *rx, uint32_t len)
{
	const struct spi_buf rx_buf = {
		.buf = rx,
		.len = len,
	};
	const struct spi_buf_set rx_set = {
		.buffers = &rx_buf,
		.count = 1,
	};

	return spi_read_dt(&ax5043_cfg_0.bus, &rx_set);
}

/**
 * Write data to the IC using the SPI
 * @param tx buffer containing the data to transmit
 * @param len the number of bytes to transmit
 * @return 0 on success or negative error code
 */
static int ax5043_spi_tx(const uint8_t *tx, uint32_t len)
{
	/* See comment in ax5043_spi_trx() */
	const struct spi_buf tx_buf = {
		.buf = (uint8_t *)tx,
		.len = len,
	};
	const struct spi_buf_set tx_set = {
		.buffers = &tx_buf,
		.count = 1,
	};

	return spi_write_dt(&ax5043_cfg_0.bus, &tx_set);
}

/**
 * Delay the execution with microsecond accuracy
 * @param us the delay in microseconds
 */
static void ax5043_usleep(uint32_t us)
{
	k_busy_wait(us);
}

/**
 * Returns the time in milliseconds. The counter should be monotonically increasing.
 * Internally the driver handles the event of a wrap around.
 * @return the time in milliseconds
 */
static uint32_t ax5043_millis(void)
{
	return k_uptime_get();
}

static void ax5043_irq_enable(bool enable)
{
#warning: trigger?
	unsigned int flags = enable ? GPIO_INT_EDGE_TO_ACTIVE : GPIO_INT_DISABLE;

	gpio_pin_interrupt_configure_dt(&ax5043_cfg_0.irq_spec, flags);
}

/**
 * Before any transmission, the driver calls this user defined function.
 * This can be used to select properly a signal path by activating a corresponding
 * RF switch and/or activate an external PA
 */
static void ax5043_tx_pre_callback(void)
{
}

/**
 * This function is automatically called by the driver at the end of every
 * transmission. In case of any error during a transmission session, the
 * driver ensures that this callback is called. Therefore it can be safely
 * used to disable any external active components like RF switches and/or
 * PA.
 *
 * @note This function is called inside the ISR so should be ISR friendly,
 * meaning that should not block and do as fast as possible its task
 */
static void ax5043_tx_post_callback(void)
{
	/* TODO: will be added later */
//	radio_tx_complete();
}

/**
 * Callback that is called when a frame is received completely.
 * @note This function is called inside the ISR so should be ISR friendly,
 * meaning that should not block and do as fast as possible its task
 *
 * @param pdu pointer to the received frame
 * @param len the number of byte received
 */
static void ax5043_rx_complete_callback(const uint8_t *pdu, size_t len)
{
	/* TODO: will be added later */
//	radio_frame_received(pdu, len);
}

int ax5043_is_ready(void)
{
	return z_device_is_ready(DEVICE_DT_INST_GET(0)) ? 0 : -ENODEV;
}
