#include <stdbool.h>

#include <toolchain.h>
#include <linker/sections.h>
#include <kernel_structs.h>
#include <kernel.h>
#include <sys/dlist.h>
#include <sys/rb.h>
#include <arch/cpu.h>
#include <kernel/thread.h>
#include <kernel/thread_stack.h>

#include "radio.h"

#define N_THREADS 1
#define STACK_SIZE (4 * 1024)
static K_THREAD_STACK_ARRAY_DEFINE(q_stacks, N_THREADS, STACK_SIZE);
static struct k_thread threads[N_THREADS];

int qubik_main(void)
{
	int ret = radio_init();

	if (ret < 0)
		return ret;

	k_thread_create(threads + 0, q_stacks[0], STACK_SIZE,
			radio_thread, NULL, NULL, NULL,
			K_HIGHEST_APPLICATION_THREAD_PRIO + 1,
			0, K_NO_WAIT);

	return 0;
}
