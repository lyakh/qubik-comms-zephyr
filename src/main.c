/*
 * Copyright (c) 2022 Guennadi Liakhovetsky <g.liakhovetski@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>

//#include <board.h>
#include <soc.h>

#include <logging/log.h>
LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

int qubik_main(void);

void main(void)
{
	int ret;

	LOG_INF("COMMS on %s", CONFIG_BOARD);

	/* qubik_main is the actual QUBIK initialisation */
	ret = qubik_main();
	if (ret)
		LOG_ERR("QUBIK initialisation failed");

	LOG_DBG("QUBIK initialised");
}
